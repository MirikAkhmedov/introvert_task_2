<?php

use Introvert\ApiClient;
use Introvert\ApiException;
use Introvert\Configuration;
use JetBrains\PhpStorm\NoReturn;

require_once('../intr-sdk-test/autoload.php');

#[NoReturn] function dd(mixed $value)
{
    echo "<pre>";
    var_dump($value);
    die();
}

/**
 * Get leads a month ahead since given date
 *
 * @param ApiClient $api
 * @param int $date
 * @param array $statuses
 * @return int[]
 *
 * @throws ApiException
 */
function getLeadsMonthAhead(ApiClient $api, int $date, array $statuses): array
{
    $result = $api->lead->getAll(null, $statuses)['result'];

    /*
     * $count = [
        '2021-08-10' => 1,
        '2021-08-12' => 0,
        '2021-08-18' => 5,
        '2021-08-20' => 8
     *  ];
     */

    $count = [];

    foreach ($result as $item) {
        $date = gmdate('Y-m-d', $item['date_close']);

        if (isset($count[$date])) {
            $count[$date]++;
        } else {
            $count[$date] = 1;
        }
    }

    return $count;
}

$key = '23bc075b710da43f0ffb50ff9e889aed';

Configuration::getDefaultConfiguration()->setApiKey('key', $key);
$api = new ApiClient();

try {
    $dates = getLeadsMonthAhead($api, '1628624379', [142]);
} catch (ApiException $e) {
    echo $e->getMessage();
}

include_once "./index.html";